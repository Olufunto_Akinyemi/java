package folderwalker;
import java.util.logging.*;

import folderwalker.XReader;

public class XLogger {

	static Logger logger = Logger.getLogger("MyLog");
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
	try {
		FileHandler fh = new FileHandler ("./Output/ServerlogFile.log");
	
		XReader walker = new XReader();
		logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);
		XReader walker2 = new XReader();
		
		final long startTime = System.currentTimeMillis();
		String path = "./Sample Data/";
		String res = walker2.walk(path, logger);
		final long endTime = System.currentTimeMillis();
		long result = endTime - startTime;
		System.out.println("Total number of seconds " + result + "ms");
		walker.writeFile(res, logger);
		

	}
	catch (Exception e ) {
		
	}

}
}