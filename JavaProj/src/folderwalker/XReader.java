package folderwalker;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
public class XReader {

	String sb = new String();
	int skippedRows = 0;
	int addedRows = 0;
	int totalRows = 0;

	public String walk(String path, Logger logger) {
		try {
			File root = new File(path);
			File[] list = root.listFiles();
			if (list == null) {
				return "";
			}

			for (File f : list) {
				if (f.isDirectory()) {
					walk(f.getAbsolutePath(), logger);
					System.out.println("Dir" + f.getAbsoluteFile());
				} else {
					System.out.println("File:" + f.getAbsoluteFile());
					if (!f.getAbsoluteFile().getName().equals(".DS_Store")) {
						Reader in;
						int iterator = 0;

						try {
							in = new FileReader(f.getAbsoluteFile().toString());
							Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);

							for (CSVRecord record : records) {

								if (iterator == 0) {
									iterator++;
									continue;
								}
								String FirstName = record.get(0);
								String LastName = record.get(1);
								String StreetNumber = record.get(2);
								String City = record.get(3);
								String Province = record.get(4);
								String Country = record.get(5);
								String PostalCode = record.get(6);
								String PhoneNumber = record.get(7);
								String EmailAddress = record.get(8);
								if (!FirstName.isEmpty() && !LastName.isEmpty() && !StreetNumber.isEmpty() && !City.isEmpty() && !Province.isEmpty() && !Country.isEmpty() && !PostalCode.isEmpty() && !PhoneNumber.isEmpty() &&!EmailAddress.isEmpty()) 
									
								{
									addedRows = addedRows + 1;
									sb += FirstName + "," + LastName + "," + StreetNumber + ","+ City +","+ Province +","+Country+"," +PostalCode+","+PhoneNumber+","+EmailAddress+"\n";

								} else {
									skippedRows = skippedRows + 1;
								}
							}
							totalRows = totalRows + 1;

						} catch (Exception e) {

						} finally {
							logger.info(skippedRows + " rows Skipped  ");
							logger.info(addedRows + "rows added");
							logger.info(totalRows + "rows in total");
						}
					}
				}
			}
		} catch (Exception e) {
		}
		return sb;

	}

	public void writeFile(String newFile, Logger logger) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("consolidated.csv"));
			//First Name	 Last Name	 Street Number	 City	 Province	 Country	 Postal Code	 Phone Number 	 email Address
			String csv = "FirstName, LastName, StreetNumber,City,Province,Country,PostalCode,PhoneNumber,EmailAddress\n";
			out.write(csv);
			out.write(newFile);
			out.close();
		} catch (IOException e) {
			logger.info("Error writing to file");

		}
	}

}

